package com.hipo.home.flickr.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.hipo.home.flickr.Listener.EndlessRecyclerOnScrollListener;
import com.hipo.home.flickr.R;
import com.hipo.home.flickr.adapter.PhotosCardViewAdapter;
import com.hipo.home.flickr.model.Photo;
import com.hipo.home.flickr.model.network.FlickrClient;
import com.hipo.home.flickr.model.network.response.PhotoResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchActivity extends AppCompatActivity {
    RecyclerView recyclerView ;
    public List<Photo> photos = new ArrayList<>();
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private String searchString = "";
    private int perPage = 100;
    private Snackbar loodingSnackBar;
    public FlickrClient flickrClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            searchString = bundle.getString("searchString");
        }

        flickrClient = MainActivity.flickrClient;
        setContentView(R.layout.activity_search);
        loodingSnackBar = Snackbar.make(findViewById(R.id.coordinatorLayoutSearch),
                getResources().getString(R.string.loading), Snackbar.LENGTH_LONG);
        loodingSnackBar.show();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_search);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new PhotosCardViewAdapter(photos,this);
        getPhotos(perPage,1);
        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener((LinearLayoutManager) layoutManager,perPage) {
            @Override
            public void onLoadMore(int current_page) {
                getPhotos(perPage,current_page);
                loodingSnackBar = Snackbar.make(findViewById(R.id.coordinatorLayoutSearch),
                        getResources().getString(R.string.loading), Snackbar.LENGTH_LONG);
                loodingSnackBar.show();
            }
        });
        recyclerView.setAdapter(mAdapter);

    }
    public void getPhotos(int perpage , int page){
        //method -- api key -- format=json --no jsoncallback=1
        flickrClient.getAppService().searchPhotos("flickr.photos.search", flickrClient.getApi_key() , searchString ,perpage,page, "json",1, new Callback<PhotoResponse>(){

            @Override
            public void success(PhotoResponse photoResponse, Response response) {
                Log.d("SearchActivity", "Success");
                if(photoResponse.getStat().equals("ok")){
                    photos.addAll( photoResponse.getPhotos().getPhotoList());
                    mAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("SearchActivity", "Failure");
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){

        if(menuItem.getItemId()==android.R.id.home)
            finish();
        return true;
    }

}
