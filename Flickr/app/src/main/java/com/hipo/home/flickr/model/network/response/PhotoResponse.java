package com.hipo.home.flickr.model.network.response;

/**
 * Created by Home on 18.04.2017.
 */
import com.google.gson.annotations.SerializedName;

import com.hipo.home.flickr.model.Photos;

public class PhotoResponse {


    @SerializedName("photos")
    Photos photos;
    @SerializedName("stat")
    String stat ;
    public Photos getPhotos() {
        return photos;
    }

    public String getStat() {
        return stat;
    }
}
