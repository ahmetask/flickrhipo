package com.hipo.home.flickr.view;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hipo.home.flickr.R;
/**
 * Created by Home on 18.04.2017.
 */

public class PhotosViewHolder extends RecyclerView.ViewHolder {
    private View view;
    public ImageView imageviewfirst;
    public ImageView imageviewsecond;
    public TextView firstimagetext;
    public TextView secondimagetext;
    public CardView card ;

    public PhotosViewHolder(View itemView) {
        super(itemView);
        view = itemView;

        imageviewfirst = (ImageView)view.findViewById(R.id.firstimage);
        imageviewsecond = (ImageView) view.findViewById(R.id.secondimage);
        card = (CardView) view.findViewById(R.id.card);
        firstimagetext = (TextView) view.findViewById(R.id.firstimagetext);
        secondimagetext = (TextView) view.findViewById(R.id.secondimagetext);
    }
}
