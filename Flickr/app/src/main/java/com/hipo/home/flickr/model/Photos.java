package com.hipo.home.flickr.model;

import com.google.gson.annotations.SerializedName;


import java.util.List;

/**
 * Created by Home on 18.04.2017.
 */


public class Photos {


    @SerializedName("page")
    Integer page;
    @SerializedName("pages")
    Integer pages;
    @SerializedName("perpage")
    Integer perpage;
    @SerializedName("total")
    Integer total;
    @SerializedName("photo")
    List<Photo> photoList;

    public List<Photo> getPhotoList(){
        return photoList;
    }
    public Integer getPage() {
        return page;
    }

    public Integer getPages() {
        return pages;
    }

    public Integer getPerpage() {
        return perpage;
    }

    public Integer getTotal() {
        return total;
    }

}
