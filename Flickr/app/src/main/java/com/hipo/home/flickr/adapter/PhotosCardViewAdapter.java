package com.hipo.home.flickr.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import com.hipo.home.flickr.R;
import com.hipo.home.flickr.activity.PopUpImageActivity;
import com.hipo.home.flickr.model.Photo;
import com.hipo.home.flickr.view.PhotosViewHolder;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Home on 18.04.2017.
 */

public class PhotosCardViewAdapter extends RecyclerView.Adapter<PhotosViewHolder> {
    private Context context;
    private View view;
    private List<Photo> photos;

    public PhotosCardViewAdapter(List<Photo> photos ,Context context){
        this.photos = photos;
        this.context = context;
    }
    @Override
    public PhotosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_mainactivity, null);
        PhotosViewHolder viewHolder = new PhotosViewHolder(view);
        return viewHolder;

    }

    @Override
    public void onBindViewHolder(PhotosViewHolder holder, int position) {

        int i =position*2;

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        if(i+1 < getItemCount()){
            final Photo photo = photos.get(i);
            final String firstimagelink = "https://farm"+photo.getFarm().toString()+".staticflickr.com/"+photo.getServer().toString()+"/"+photo.getId().toString()+"_"+photo.getSecret()+"_q.jpg";
            if(position+1<getItemCount()){
                final Photo photo2 = photos.get(i+1);
                final String secondimagelink = "https://farm"+photo2.getFarm().toString()+".staticflickr.com/"+photo2.getServer().toString()+"/"+photo2.getId().toString()+"_"+photo2.getSecret()+"_q.jpg";
                Picasso.with(context).load(secondimagelink).resize(width/2,400)
                        .centerCrop().into(holder.imageviewsecond);
                holder.secondimagetext.setText(context.getResources().getString(R.string.picture)+ Integer.toString(i+2) );
                holder.imageviewsecond.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        String secondImageOriginalSize =  "https://farm"+photo2.getFarm().toString()+".staticflickr.com/"+photo2.getServer().toString()+"/"+photo2.getId().toString()+"_"+photo2.getSecret()+"_b.jpg";
                        Intent i = new Intent(context,PopUpImageActivity.class);
                        i.putExtra("img",secondImageOriginalSize);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);


                    }
                });
            }
            Picasso.with(context).load(firstimagelink).resize(width/2, 400)
                    .centerCrop().into(holder.imageviewfirst);
            holder.firstimagetext.setText(context.getResources().getString(R.string.picture)+ Integer.toString(i+1) );
            holder.imageviewfirst.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                     String firstImageOriginalSize = "https://farm"+photo.getFarm().toString()+".staticflickr.com/"+photo.getServer().toString()+"/"+photo.getId().toString()+"_"+photo.getSecret()+"_b.jpg";
                    Intent i = new Intent(context,PopUpImageActivity.class);
                    i.putExtra("img",firstImageOriginalSize);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);


                }
            });
        }
    }

    @Override
    public void onViewDetachedFromWindow(PhotosViewHolder holder){
        super.onViewDetachedFromWindow(holder);
        holder.imageviewfirst.setImageDrawable(null);
        holder.imageviewsecond.setImageDrawable(null);
        holder.secondimagetext.setText("");
        holder.firstimagetext.setText("");

    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if(photos !=null)
            return photos.size();
        else
            return 0;
    }
}
