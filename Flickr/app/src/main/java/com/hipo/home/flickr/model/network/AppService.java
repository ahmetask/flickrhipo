package com.hipo.home.flickr.model.network;


import com.hipo.home.flickr.model.network.response.PhotoResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Home on 18.04.2017.
 */

public interface AppService {
    @GET("/rest")
    void getPhotos(@Query("method") String method , @Query("api_key") String api_key , @Query("per_page") Integer  per_page , @Query("page") Integer page , @Query("format") String format , @Query("nojsoncallback") Integer nojsoncallback , Callback<PhotoResponse> callback);
    @GET("/rest")
    void searchPhotos(@Query("method") String method, @Query("api_key") String api_key, @Query("tags") String tags ,@Query("per_page") Integer  per_page , @Query("page") Integer page , @Query("format")  String format, @Query("nojsoncallback") Integer nojsoncallback, Callback<PhotoResponse> callback);
}

