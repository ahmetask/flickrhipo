package com.hipo.home.flickr.model;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Home on 18.04.2017.
 */


public class Photo {


    @SerializedName("id")
    String id;
    @SerializedName("owner")
    String owner;
    @SerializedName("secret")
    String secret;
    @SerializedName("server")
    Integer server;
    @SerializedName("farm")
    Integer farm;
    @SerializedName("title")
    String title;
    @SerializedName("ispublic")
    int ispublic;
    @SerializedName("isfriend")
    int isfriend;
    @SerializedName("isfamily")
    int isfamily;

    public Integer getFarm() {
        return farm;
    }
    public String getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public String getSecret() {
        return secret;
    }

    public Integer getServer() {
        return server;
    }

    public String getTitle() {
        return title;
    }

    public int getIspublic() {
        return ispublic;
    }

    public int getIsfriend() {
        return isfriend;
    }

    public int getIsfamily() {
        return isfamily;
    }



}