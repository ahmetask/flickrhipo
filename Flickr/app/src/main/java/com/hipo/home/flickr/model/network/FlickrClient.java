package com.hipo.home.flickr.model.network;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Home on 18.04.2017.
 */

public class FlickrClient {
    private String API_URL = "https://api.flickr.com/services";
    private AppService appService;
    private String api_key = "ac5f42163882d85c1f4a867083217947";
    public FlickrClient(){
        RestAdapter retrofit = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(API_URL)
                .setConverter(new GsonConverter(getGsonConvertor()))
                .build();
        appService = retrofit.create(AppService.class);

    }
    public AppService getAppService() {
        return appService;
    }
    private Gson getGsonConvertor() {
        GsonBuilder builder = new GsonBuilder();
        return builder.create();
    }
    public String getApi_key(){
        return api_key;
    }

}
