package com.hipo.home.flickr.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.hipo.home.flickr.Listener.EndlessRecyclerOnScrollListener;
import com.hipo.home.flickr.R;
import com.hipo.home.flickr.adapter.PhotosCardViewAdapter;
import com.hipo.home.flickr.core.Cache;
import com.hipo.home.flickr.model.Photo;
import com.hipo.home.flickr.model.SearchHistory;
import com.hipo.home.flickr.model.network.FlickrClient;
import com.hipo.home.flickr.model.network.response.PhotoResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {
    public static FlickrClient flickrClient ;
    private RecyclerView recyclerView ;
    private List<Photo> photos = new ArrayList<>();
    private RecyclerView.Adapter mAdapter;
    private LinearLayoutManager layoutManager;
    private SearchHistory searchHistory;
    private ArrayAdapter<String> histAdapter;
    private  ListView histListView;
    private  int perPage = 100;
    private Snackbar loodingSnackBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        flickrClient = new FlickrClient();

        loodingSnackBar = Snackbar.make(findViewById(R.id.coordinatorLayout),
                getResources().getString(R.string.loading), Snackbar.LENGTH_LONG);
        loodingSnackBar.show();
        getPhotos(perPage,1);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);




        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener( layoutManager ,perPage) {
            @Override
            public void onLoadMore(int current_page) {

                getPhotos(perPage,current_page);
                loodingSnackBar = Snackbar.make(findViewById(R.id.coordinatorLayout),
                        getResources().getString(R.string.loading), Snackbar.LENGTH_LONG);
                loodingSnackBar.show();

            }
        });

        mAdapter = new PhotosCardViewAdapter(photos,this);
        recyclerView.setAdapter(mAdapter);


        searchHistory  = new SearchHistory();
        try {
            // Retrieve the list from internal storage
            SearchHistory sh = (SearchHistory) Cache.readObject(this, "hist");
            searchHistory.getHistList().addAll(sh.getHistList());
            // Display the items from the list retrieved.
            for (String entry : searchHistory.getHistList()) {
                Log.d("readSearchHist", entry);
            }
        } catch (IOException e) {
            Log.e("IOException", e.getMessage());
        } catch (ClassNotFoundException e) {
            Log.e("IOException", e.getMessage());
        }
        histListView = (ListView)findViewById(R.id.list_view);
        histListView.setVisibility(View.GONE);
        histAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, android.R.id.text1, searchHistory.getHistList());
        histListView.setAdapter(histAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem menuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menuItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                Log.d("Submit Search Text", query);
                histAdapter.add(query);
                searchHistory.getHistList().add(query);
                try {
                    // Save the list of entries to internal storage
                    Cache.writeObject(getBaseContext(), "hist", searchHistory);
                    for (String entry : searchHistory.getHistList()) {
                        Log.d("writeSearchHist", entry);
                    }

                } catch (IOException e) {
                    Log.e("IOException", e.getMessage());
                }

                histAdapter.notifyDataSetChanged();
                query = query.replaceAll(" ", "+");
                getSearchPhotos(query);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                         //histListView.setAdapter(histAdapter);
                Log.d("Search Text Change", newText);
                histAdapter.getFilter().filter(newText);
                return false;
            }
        });

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                histListView.setVisibility(View.VISIBLE);
                histListView.bringToFront();
            }
        });
        MenuItem searchItem = menu.findItem(R.id.action_search);

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                histListView.setVisibility(View.VISIBLE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Write your code here
                histListView.setVisibility(View.GONE);
                return true;
            }
        });
        histListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,long id) {

                String text = histListView.getItemAtPosition(position).toString();
                searchView.setQuery(text, false);
                histListView.setVisibility(View.GONE);


            }
        });
        histListView.setVisibility(View.GONE);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getSearchPhotos(String tags){
        Intent i = new Intent(getBaseContext(),SearchActivity.class);
        i.putExtra("searchString",tags);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getBaseContext().startActivity(i);
    }


    public void getPhotos(final int perpage , final int page){
        //method -- api key -- perpage --page -- format --no jsoncallback =1
        flickrClient.getAppService().getPhotos("flickr.photos.getRecent",flickrClient.getApi_key(),perpage, page,"json", 1 ,new Callback<PhotoResponse>(){
            @Override
            public void success(PhotoResponse photoResponse, Response response) {
                Log.d("MainActivity", "Success");
                if(photoResponse !=null){
                    if(photoResponse.getStat().equals("ok")){
                        photos.addAll(photoResponse.getPhotos().getPhotoList());
                        mAdapter.notifyDataSetChanged();
                    }
                }
            }
            @Override
            public void failure(RetrofitError error) {
                Log.d("MainActivity", "Failure");
            }
        });
    }


}
