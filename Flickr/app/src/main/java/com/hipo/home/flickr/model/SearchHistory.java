package com.hipo.home.flickr.model;

/**
 * Created by Home on 20.04.2017.
 */


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class SearchHistory implements Serializable {

    public List<String> histList;

    public  SearchHistory(){
        histList = new ArrayList<>();

    }

    public  List<String> getHistList(){

         return histList;
    }

}
